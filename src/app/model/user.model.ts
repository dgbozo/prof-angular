export interface AppUser{
    userid: string;
    username: string;
    password: string;
    role: string[];
}
 
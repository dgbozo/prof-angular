export interface Product{
    id: string;
    name: string;
    prix: number;
    promotion: boolean;
}
export interface Prod{
    id: string;
    name: string;
    price: number;
    checked: boolean;
}
export interface PageProduct{
    products:Product[],
    page: number,
    size:number,
    totalPages:number
}
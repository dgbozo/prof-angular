import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'catalogue';
  actions: Array<any>=[{
    title:"home",route:"/home",icon:"house"
  },
  {
    title:"test1",route:"/new-product",icon:"safe"
  },
  {
    title:"test2",route:"/test2",icon:"search"
  }];
currentAction:any;
handleAction(action: any) {
  this.currentAction=action;
  }
}

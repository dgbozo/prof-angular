import { Injectable } from '@angular/core';
import { AppUser } from '../model/user.model';
import { UUID } from 'angular2-uuid';
import { Observable, of, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthentificationService {
users:AppUser[]=[];
authentificadUser :AppUser | undefined;
  constructor() {

    this.users.push({userid: UUID.UUID(),username:"user1",password:"1234",role: ["USER"]});
    this.users.push({userid: UUID.UUID(),username:"user2",password:"1234",role: ["USER"]});
    this.users.push({userid: UUID.UUID(),username:"admin",password:"1234",role: ["USER","ADMIN"]});
   }
   public login(username:string,password: string ):Observable<AppUser>{
let appUser=this.users.find(u=>u.username==username);
if(!appUser) return throwError(()=>new Error("User not find"))
if(appUser.password!=password){
  return throwError(()=>new Error("BAd credential "))
}return of(appUser);
   }


   public authenticateUser(appUser :AppUser):Observable<boolean>{
    this.authentificadUser=appUser;
    localStorage.setItem("authUser",JSON.stringify({username:appUser.username,role:appUser.role,jwt:"JWT_TOKEN"}));
    return of(true);

   }

   public hasRole(role: string):boolean{
   return  this.authentificadUser!.role.includes(role);
   }

   public isAuthenticated(){
    return this.authentificadUser!=undefined;
   }
   public logout():Observable<boolean>{
    this.authentificadUser=undefined;
    localStorage.removeItem("authUser");
    return of(true);
   }
}

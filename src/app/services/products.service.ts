import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { PageProduct, Prod, Product } from '../model/product.model';
import { UUID } from 'angular2-uuid';
import { ValidationErrors } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
 private  products! : Array<Product> ;
  constructor(private http: HttpClient) {
    this.products=[

      {id:UUID.UUID(),name:"computer",prix:3000,promotion:false},
      {id:UUID.UUID(),name:"computer",prix:3000,promotion:true},
  
      {id:UUID.UUID(),name:"linux",prix:3000,promotion:false},
  
      {id:UUID.UUID(),name:"computer",prix:3000,promotion:false},

  
    ];
    for (let i = 0; i < 10; i++) {
      this.products.push({id:UUID.UUID(),name:"hp",prix:3000,promotion:false});
      this.products.push({id:UUID.UUID(),name:"dell",prix:330000,promotion:true});
   
  
   
    }
   }
public getALlTest1():Observable<Array<Prod>>{
  return   this.http.get<Array<Prod>>("http://localhost:3000/products");
}
public setCheckedTest1(product:Prod):Observable<any>{
  return this.http.patch<Prod>(`http://localhost:3000/products/${product.id}`,{checked:!product.checked});
}
public deleteTest1(product:Prod){
  return this.http.delete<Prod>(`http://localhost:3000/products/${product.id}`);
}


public addNewTest1(product :Prod): Observable<Prod>{
 // product.id=UUID.UUID();
 return this.http.post<Prod>("http://localhost:3000/products",product);
  
  }
  
   public getAllProduct() : Observable<Product[]>{
   // let rnd=Math.random();
 //   if (rnd<0.5) return throwError(()=>new Error("INternet error"));
    
   // else
     return of([...this.products]);

   }
     
   public getPageProduct(page :number, size :number) : Observable<PageProduct>{
    let index= page*size;
    let totalPages= ~~(this.products.length / size);
    if (this.products.length % size !=0) 
        totalPages++;
    let pageProducts=this.products.slice(index,index+size);
    return of({page:page, size:size,totalPages:totalPages,products : pageProducts});
    
    // let rnd=Math.random();
  //   if (rnd<0.5) return throwError(()=>new Error("INternet error"));
     
     
 
    }

   public deleteProduct(id:string):Observable<boolean>{
this.products=this.products.filter(p=>p.id!=id);
return of(true);
   }

   public setPromotion(id : string) : Observable<boolean> {
    let product = this.products.find(p=>p.id==id);
    if (product !=undefined) {
      product.promotion=!product.promotion; 
      return of(true);
    } else return throwError(()=>new Error("Product not found "));
    
   }

public searchProducts( keyword :string,page:number,size:number) :Observable<PageProduct>{

let products= this.products.filter(p=>p.name.includes(keyword));
let index=page*size;
let totalPages= ~~(products.length / size);
if (this.products.length % size !=0) 
  totalPages++;
let pageProducts=products.slice(index,index+size);
return of({page:page, size:size,totalPages:totalPages,products : pageProducts});

 
}

public addNewProduct(product :Product): Observable<Product>{
product.id=UUID.UUID();
this.products.push(product);
return of(product);
}
public getProduct(id:string):Observable<Product>{
 let product= this.products.find(p=>p.id==id);
 if (product==undefined) return throwError(()=>new Error ("product not find"))
  
 
 return of(product); 
}


getErrorMessage(fieldName: string , error: ValidationErrors) : string {
  if (error['required']) {
    return fieldName + "is required";
  }else if (error['minLenght']) {
    return fieldName + " should have at least"+ error['minlenght']['requiredLenght']+" characters";
  } else if (error['min']) {
    return fieldName + " should have min value"+ error['min']['min']+" characters";
  }else return"";
  }

  public updateProduct(product:Product):Observable<Product>{
    this.products=this.products.map(p=>(p.id==product.id)?product:p);
    return of(product);
  }

  
}

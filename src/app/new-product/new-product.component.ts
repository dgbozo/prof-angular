import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { AuthentificationService } from '../services/authentification.service';
import { Router } from '@angular/router';
import { ProductsService } from '../services/products.service';

@Component({
  selector: 'app-new-product',
  templateUrl: './new-product.component.html',
  styleUrls: ['./new-product.component.css']
})
export class NewProductComponent implements OnInit {


  productFormGroup! : FormGroup;
  constructor(private fb : FormBuilder,public  authService: AuthentificationService ,public router:Router,public productService: ProductsService){

  }

  ngOnInit(){
    this.productFormGroup=this.fb.group({
      name:this.fb.control(null,[Validators.required,Validators.minLength(4)]),
      price:this.fb.control(0,[Validators.required,Validators.min(200)]),
      checked:this.fb.control(false,[Validators.required])
    })
  }

  handleAddProduct() {

   

    let product=this.productFormGroup.value;

    this.productService.addNewTest1(product).subscribe({
      next :(data)=>{
alert("Product added successflly");
this.productFormGroup.reset;
      },error : err=>{
console.log(err);
      }

    })
      }
    

}

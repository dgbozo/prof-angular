import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../services/products.service';
import { Prod } from '../model/product.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  products: Array<Prod>=[];
constructor(private pS:ProductsService){

}

ngOnInit(): void {
 this.AllProduct();
}
AllProduct(){
  this.pS.getALlTest1().subscribe({
    next:data=>{
      this.products=data
    },error:err=>{
      console.log(err);
    }
  });
}

handleSetProduct(product: Prod) {
  this.pS.setCheckedTest1(product).subscribe({
    next: updateProduct=>{
      product.checked=!product.checked;
    }
  });
 
  }

  handleDeleteProduct(product: Prod) {
    if (confirm("Est vous sur ?")) {
      this.pS.deleteTest1(product).subscribe({
        next: value=>{
          //this.AllProduct();
          this.products=this.products.filter(p=>p.id!=product.id);
        }
      });
    }

    }
}

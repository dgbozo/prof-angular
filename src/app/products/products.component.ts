import { Component } from '@angular/core';
import { ProductsService } from '../services/products.service';
import { Product } from '../model/product.model';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AuthentificationService } from '../services/authentification.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent {
handleEditProduct(p: Product) {
this.router.navigateByUrl("/admin/editProduct/"+p.id);
}


products! : Array<Product>;
currentPage : number=0;
pageSize:number=2;
totaPages:number=0;
errorMessage! : string;
 searchFormGroup! : FormGroup;



 constructor(private productService: ProductsService , private fb : FormBuilder,public  authService: AuthentificationService ,public router:Router){

}

ngOnInit():void{
  this.searchFormGroup=this.fb.group({
    keyword : this.fb.control(null)
  });
this.getAll();
 
}


handleNewProduct() {
  this.router.navigateByUrl("/admin/new-product");
  }



SearchProduct() {
  console.log("hello");
  let keyword=this.searchFormGroup.value.keyword;
  this.productService.searchProducts(keyword,this.currentPage,this.pageSize).subscribe({
    next:(data)=>{
      this.products=data.products
      this.totaPages=data.totalPages;
      console.log(data);
    }
  })

  } 
/*getAll(){
  this.productService.getAllProduct().subscribe({
    next:(data)=>{
      this.products=data;
    },error:(err)=>{
  this.errorMessage=err;
    }
  });
}*/
getAll(){
  this.productService.getPageProduct(this.currentPage,this.pageSize ).subscribe({
    next:(data)=>{
      this.products=data.products;
      this.totaPages=data.totalPages;
      console.log(this.totaPages);
    },error:(err)=>{
  this.errorMessage=err;
    }
  });
}
delete(p: Product){
  let conf=confirm("are you sure?");
  if(conf==false) return;  
 this.productService.deleteProduct(p.id).subscribe({
  next:(data)=>{
      //this.getAll();
      let index=this.products.indexOf(p);
       this.products.splice(index,1);
  }
 })
}
promotion(p : Product){
  let promo=p.promotion;
this.productService.setPromotion(p.id).subscribe({
  next:(data)=>{
    p.promotion=!promo;
  },
  error : err=>{
    this.errorMessage=err;
  }
})
}
 gotoPage(i: number){
  this.currentPage=i;
  this.getAll();
 }
}

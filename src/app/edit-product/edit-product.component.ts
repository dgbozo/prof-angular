import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductsService } from '../services/products.service';
import { Product } from '../model/product.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscriber } from 'rxjs';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent {
handleUpdateProduct() {
let p =this.productFormGroup.value;
p.id=this.product.id;

this.productService.updateProduct(p).subscribe({
  next:()=>{
    alert("Product update");
  },
  error:(err)=>{
    console.log(err);
  }
})
}
  productId!:string;
  product!:Product;
  productFormGroup!:FormGroup;
constructor( private route:ActivatedRoute,public productService:ProductsService,private fb : FormBuilder){
this.productId=this.route.snapshot.params['id'];
}
ngOnInit():void{
this.productService.getProduct(this.productId).subscribe({
  next:(product)=>{
this.product=product;
this.productFormGroup=this.fb.group({
  name:this.fb.control(this.product.name,[Validators.required,Validators.minLength(4)]),
  prix:this.fb.control(this.product.prix,[Validators.required]),
  promotion:this.fb.control(this.product.promotion,[Validators.required])
})
  },
  error:(err)=>{
    console.log(err);
  }
})
}
}
